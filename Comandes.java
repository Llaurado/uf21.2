import java.util.Date;

public class Comandes {

    int num_comanda;
    double preu_total;
    Date data;
    String dni_client;

    public Comandes(int num_comanda, double preu_total, Date data, String dni_client) {
        this.num_comanda = num_comanda;
        this.preu_total = preu_total;
        this.data = data;
        this.dni_client = dni_client;
    }

    public int getNum_comanda() {
        return num_comanda;
    }

    public void setNum_comanda(int num_comanda) {
        this.num_comanda = num_comanda;
    }

    public double getPreu_total() {
        return preu_total;
    }

    public void setPreu_total(double preu_total) {
        this.preu_total = preu_total;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getDni_client() {
        return dni_client;
    }

    public void setDni_client(String dni_client) {
        this.dni_client = dni_client;
    }

    @Override
    public String toString() {
        return "Comandes{" +
                "num_comanda=" + num_comanda +
                ", preu_total=" + preu_total +
                ", data=" + data +
                ", dni_client='" + dni_client + '\'' +
                '}';
    }
}
