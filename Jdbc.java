import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;

public class Jdbc {
    private static Connection myconn;
    private static ArrayList<Client> clientlist = new ArrayList<>();
    private static ArrayList<Comandes> comandalsit = new ArrayList<>();

    static {
        try {
            myconn = DriverManager.getConnection("jdbc:mysql://localhost:3306/basededatos", "root", "root");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static int menu() {
        System.out.println("Operacions:\n1.Crear taules\n2.Mostrar clients y comandes\n3.Borrar dades i inserir la llista\n4.Nova Client\n5.Alta comanda\n6.Mostrar comandes\n0.Sortir");
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();

    }

    public static void crearTaulaClient() {
        try {
            if (!myconn.getAutoCommit()) {
                throw new SQLException("error al conectar");
            }

            DatabaseMetaData dbm = myconn.getMetaData();
            ResultSet tables = dbm.getTables(null, null, "client", null);
            Statement statement = myconn.createStatement();


            if (!tables.next()) {
                String query = " create table client(nif varchar(9) primary key,nom varchar(10) ,corrupte boolean);";
                statement.execute(query);
                System.out.println("Taula creada");
            } else {
                System.out.println("La taula ja existeix");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public static void crearTaulaComandes() {
        try {
            if (!myconn.getAutoCommit()) {
                throw new SQLException("error al conectar");
            }

            DatabaseMetaData dbm = myconn.getMetaData();
            ResultSet tables = dbm.getTables(null, null, "comandes", null);
            Statement statement = myconn.createStatement();

            if (!tables.next()) {
                String query = " create table comandes(num_comanda int(9) primary key, preu_total decimal(6,2),data date, dni_client varchar(9),foreign key (dni_client) references client(nif));";
                statement.execute(query);
                System.out.println("Taula creada");
            } else {
                System.out.println("La taula ja existeix");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public static ArrayList<Client> recuperarDades() {
        ArrayList<Client> Arraylistclient = new ArrayList<>();
        Client client = null;
        try {
            Statement statement = myconn.createStatement();
            String selectclient = "select * from client";
            String selectcomandes = "select * from comandes";
            ResultSet rscomandes = statement.executeQuery(selectcomandes);
            while (rscomandes.next()) {
                Comandes comandes = new Comandes(rscomandes.getInt(1), rscomandes.getDouble(2), rscomandes.getDate(3), rscomandes.getString(4));
                comandalsit.add(comandes);
            }
            ResultSet rsclient = statement.executeQuery(selectclient);
            while (rsclient.next()) {
                client = new Client(rsclient.getString(1), rsclient.getString(2), rsclient.getBoolean(3));
                ArrayList<Comandes> comandes = new ArrayList<>();
                for (int i = 0; i < comandalsit.size(); i++) {
                    if (rsclient.getString(1).equals(comandalsit.get(i).getDni_client())) {
                        comandes.add(comandalsit.get(i));

                    }
                }
                client.setComandes(comandes);
                clientlist.add(client);
            }
            for (int i = 0; i < clientlist.size(); i++) {
                System.out.println(clientlist.get(i));
                for (int j = 0; j < clientlist.get(i).getComandes().size(); j++) {
                    if (clientlist.get(i).getNif().equals(clientlist.get(i).getComandes().get(j).getDni_client())) {
                        client = new Client(clientlist.get(i).getNif(), clientlist.get(i).getNom(), clientlist.get(i).getVip(), clientlist.get(i).getComandes());
                        Arraylistclient.add(client);
                        System.out.println(clientlist.get(i).getComandes().get(j));
                    }
                }
            }
            comandalsit.clear();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Arraylistclient;

    }

    public static void borrarDades() {

        Statement mystmn = null;
        try {
            mystmn = myconn.createStatement();
            String deleteclient = "delete from client";
            String deletecomandes = "delete from comandes";
            PreparedStatement preparedStatementclient = myconn.prepareStatement(deleteclient);
            PreparedStatement preparedStatementcomandes = myconn.prepareStatement(deletecomandes);
            preparedStatementcomandes.execute();

            preparedStatementclient.execute();
            String sentenciaSQLClient = "insert into client (nif,nom,corrupte)  values (?,?,?)";
            String sentenciaSQLComandes = "insert into comandes (num_comanda,preu_total,data,dni_client)  values (?,?,?,?)";
            PreparedStatement sentenciaPreparadaclient = myconn.prepareStatement(sentenciaSQLClient);
            PreparedStatement sentenciaPreparadacomandes = myconn.prepareStatement(sentenciaSQLComandes);

            for (int i = 0; i < clientlist.size(); i++) {
                sentenciaPreparadaclient.setString(1, clientlist.get(i).getNif());
                sentenciaPreparadaclient.setString(2, clientlist.get(i).getNom());
                sentenciaPreparadaclient.setBoolean(3, clientlist.get(i).getVip());
                sentenciaPreparadaclient.executeUpdate();
                System.out.println(clientlist.get(i));
                System.out.println("client inserti correctament");

                for (int j = 0; j < clientlist.get(i).getComandes().size(); j++) {

                    sentenciaPreparadacomandes.setInt(1, clientlist.get(i).getComandes().get(j).getNum_comanda());
                    sentenciaPreparadacomandes.setDouble(2, clientlist.get(i).getComandes().get(j).getPreu_total());
                    sentenciaPreparadacomandes.setDate(3, (java.sql.Date) clientlist.get(i).getComandes().get(j).getData());
                    sentenciaPreparadacomandes.setString(4, clientlist.get(i).getComandes().get(j).getDni_client());
                    sentenciaPreparadacomandes.executeUpdate();
                    System.out.println(clientlist.get(i).getComandes().get(j));
                    System.out.println("Comandes inserit correctament.");
                }

            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }


    public static void altaClient(Client c1) {
        boolean pk = primaryKeyClient(c1);
        if (pk) {
            clientlist.add(c1);
            System.out.println("client inserit");
            System.out.println(c1);

        } else {
            System.out.println("primary key repetida");
        }
    }


    public static boolean primaryKeyClient(Client c1) {
        boolean primarykey = true;
        Statement statement = null;
        try {
            statement = myconn.createStatement();
            String selectclient = "select * from client";
            ResultSet rsclient = statement.executeQuery(selectclient);

            while (rsclient.next()) {
                Client client = new Client(rsclient.getString(1), rsclient.getString(2), rsclient.getBoolean(3));
                clientlist.add(client);
            }
            for (int i = 0; i < clientlist.size(); i++) {
                if (clientlist.get(i).getNif().equals(c1.getNif())) {
                    primarykey = false;
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return primarykey;
    }

    public static boolean primaryKeyComandes(Comandes c1) {
        boolean primarykey = true;
        Statement statement = null;
        try {
            statement = myconn.createStatement();
            String selectclient = "select * from comandes";
            ResultSet rscomandes = statement.executeQuery(selectclient);

            while (rscomandes.next()) {
                Comandes comandes = new Comandes(rscomandes.getInt(1), rscomandes.getDouble(2), rscomandes.getDate(3), rscomandes.getString(4));
                comandalsit.add(comandes);
            }
            for (int i = 0; i < comandalsit.size(); i++) {
                if (comandalsit.get(i).getNum_comanda() == c1.getNum_comanda()) {
                    primarykey = false;
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return primarykey;

    }

    public static void altacomanda(Comandes comandes) {
        llegirClient();
        Scanner sc = new Scanner(System.in);
        System.out.println("Escriu el dni del client seleccionat");
        String dniclient = sc.next();
        comandes.setDni_client(dniclient);

        boolean pk = primaryKeyComandes(comandes);
        if (pk) {
            comandalsit.add(comandes);
            System.out.println("comanda inserida correctament");
        } else {
            System.out.println("primary key repetida");
        }

    }

    public static Comandes mostrarcom() {
        Scanner sc = new Scanner(System.in);
        Comandes comandes = null;
        Statement statement = null;
        try {

            System.out.println("Escriu el dni del client");
            String clientdni = sc.next();
            statement = myconn.createStatement();

            String selectcomandes = "select * from comandes";
            ResultSet rscomandes = statement.executeQuery(selectcomandes);
            while (rscomandes.next()) {
                if (clientdni.equals(rscomandes.getString(4))) {
                    comandes = new Comandes(rscomandes.getInt(1), rscomandes.getDouble(2), rscomandes.getDate(3), rscomandes.getString(4));
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return comandes;
    }

    public static void llegirClient() {
        Statement statement = null;
        try {
            statement = myconn.createStatement();

            String selectclient = "select * from client";
            ResultSet rsclient = statement.executeQuery(selectclient);
            int i = 0;
            while (rsclient.next()) {
                System.out.println(i + ") " + rsclient.getString(1) + ", " + rsclient.getString(2));
                i++;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void main(String[] args) throws SQLException, ParseException {
        Scanner sc = new Scanner(System.in);
        int opcio = menu();
        while (opcio != 0) {
            switch (opcio) {
                case 1:
                    crearTaulaClient();
                    crearTaulaComandes();

                    break;
                case 2:
                    recuperarDades();

                    break;
                case 3:
                    borrarDades();

                    break;
                case 4:


                    Client c1;
                    String dni;
                    String nom;
                    boolean vip;

                    System.out.println("Alta Client-----\n1.dni(sense lletra)");
                    dni = sc.next();
                    System.out.println("2.nom");
                    nom = sc.next();
                    System.out.println("3.Es premium?(True or false)");
                    vip = sc.nextBoolean();

                    c1 = new Client(dni, nom, vip, null);
                    altaClient(c1);
                    break;
                case 5:

                    System.out.println("Alta Client-----\n1.num_com");
                    int num_com = sc.nextInt();

                    System.out.println("2.preu (2 decimals XX,XX)");
                    double preu = sc.nextDouble();

                    System.out.println("3.data dd/MM/yyy");
                    String data = sc.next();

                    Date a = new SimpleDateFormat("dd/MM/yyy").parse(data);
                    Comandes comandes = new Comandes(num_com, preu, a, null);

                    altacomanda(comandes);
                    break;
                case 6:
                    llegirClient();
                    System.out.println(mostrarcom());
                    break;

            }

            opcio = menu();
        }
        myconn.close();
    }
}




